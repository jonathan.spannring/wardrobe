import React, { useEffect, useState } from 'react';


function HatForm () {
    const [formData, setFormData] = useState({});
    const [locations, setLocations] = useState([]);

    const getLocations = async function() {
        const url = 'http://localhost:8100/api/locations';
        try {
            const res = await fetch(url);
            if(res.ok) {
                const { locations } = await res.json();
                setLocations(locations);
            }
        } catch(e) {
            console.log("There was an error fetching locations", e);
        }
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const url = 'http://localhost:8090/hats/';
        try{
            const fetchOptions = {
                method: 'post',
                body: JSON.stringify(formData),
                headers: {
                    'Content-Type': 'application/json'
                }
            }
            const res = await fetch(url, fetchOptions);
            if (res.ok) {
                setFormData({
                    fabric: '',
                    style: '',
                    color: '',
                    picture_url: '',
                    location: '',
                })
            }
        } catch(e) {
            console.log('An error has occured when POSTing a hat', e);
        }
    }


  useEffect(() => {
    getLocations();
  }, []);

  //Shortens code so I don't have to type each change individually
  const handleFormChange = function({ target }) {
    const { value, name } = target;

    setFormData({
        ...formData,
        [name]: value
    });
  }

  const {
    fabric,
    style,
    color,
    picture_url:pictureUrl,
    location,
  } = formData;

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Create a new hat</h1>
          <form onSubmit={handleSubmit}>

            <div className="form-floating mb-3">
              <input value={fabric} onChange={handleFormChange} type="text" className="form-control" name="fabric"/>
              <label htmlFor="fabric"className="form-label">Fabric</label>
            </div>

            <div className="form-floating mb-3">
              <input value={style} onChange={handleFormChange} type="text" className="form-control" name="style"/>
              <label htmlFor="style"className="form-label">Style</label>
            </div>

            <div className="form-floating mb-3">
              <input value={color} onChange={handleFormChange} type="text" className="form-control" name="color"/>
              <label htmlFor="color"className="form-label">Color</label>
            </div>

            <div className="form-floating mb-3">
              <input value={pictureUrl} onChange={handleFormChange} type="text" className="form-control" name="picture_url"/>
              <label htmlFor="picture_url"className="form-label">Picture URL</label>
            </div>

            <div className="mb-3">
              <label htmlFor="location" className="form-label">Location</label>
              <select value={location} onChange={handleFormChange} className="form-control" name="location">
                <option value="">Choose a location</option>
                {locations.map(({id, closet_name, section_number}) => (
                    <option value={id} key={id + closet_name}>{closet_name} - Location {section_number}</option>
                ))}
              </select>
            </div>
            <button className="btn btn-primary">Create</button>
          </form>
        </div>
      </div>
    </div>
  )
}


export default HatForm;





// const data = {};
// data.fabric = fabric;
// data.style = style;
// data.color = color;
// data.picture_ulr = picture_url;
// data.location = location;

// const locationUrl = 'http://localhost:8000/api/locations/';
// const fetchConfig = {
//   method: "post",
//   body: JSON.stringify(data),
//   headers: {
//     'Content-Type': 'application/json',
//   },
// };

// const response = await fetch(locationUrl, fetchConfig);
// if (response.ok) {
//   const newLocation = await response.json();
//   console.log(newLocation);

//   setFabric('');
//   setStyle('');
//   setColor('');
//   setPictureUrl('');
//   setLocation('');
// }
// }



//   // How can we refactor these handleChange methods to make
//   // a single method, like the ConferenceForm above?
//   const handleFabricChange = (event) => {
//     const value = event.target.value;
//     setFabric(value);
//   }

//   const handleStyleChange = (event) => {
//     const value = event.target.value;
//     setStyle(value);
//   }

//   const handleColorChange = (event) => {
//     const value = event.target.value;
//     setColor(value);
//   }

//   const handlePictureUrlChange = (event) => {
//     const value = event.target.value;
//     setPictureUrl(value);
//   }

//   const handleLocationChange = (event) => {
//     const value = event.target.value;
//     setLocation(value);
//   }
