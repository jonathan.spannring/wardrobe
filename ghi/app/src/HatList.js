import React, { useEffect, useState } from 'react';
import HatListItem from './HatListItem';
import { NavLink } from 'react-router-dom';

const HatList = function(props) {
    const [hats, setHats] = useState([]);

    const getHats = async function() {
        const url = 'http://localhost:8090/hats/';
        const response = await fetch(url);
        const data = await response.json();
        setHats(data.hats);
        console.log(data);
    }

    const deleteHat = async function(id) {
        const url = `http://localhost:8090/hats/${id}/`
        try {
            const res = await fetch(url, {method: 'delete'})
            if (res.ok) {

                const newHats = hats.filter(hat => hat.id !== id);
                setHats(newHats);
            }
        } catch(e) {
            console.log('An error occured deleting the hat', e);
        }
    }

    useEffect(() => {
        getHats();

    },[])

    return(
        <>
            <h1 style={{ textAlign: 'center' }}>Hats</h1>
            <NavLink className="navbar-nav me-auto mb-2 mb-lg-0 create-hat-button" to="/hats/new">Create Hat</NavLink>
            <div className="row">
                {hats.map(hat => <HatListItem key={hat.id} hat={hat} deleteHat={deleteHat} />)}
            </div>
        </>

    )
}

  export default HatList;
