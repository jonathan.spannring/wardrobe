import React from 'react';
import { useParams } from 'react-router-dom';


// hats data is not transfering like HatListItem
// pulling the data using params and parseInt
//useParams is a hook that extracts hatId
const HatDetail = function({ hats }) {
  const { hatId } = useParams();
// find() finds the hat in hats and parseInt turns hatId into a number to compare against the hat.id
  const hat = hats.find((hat) => hat.id === parseInt(hatId, 10));

    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Fabric</th>
                <th>Style</th>
                <th>Color</th>
                <th>Picture</th>
                <th>Location</th>
            </tr>
            </thead>
            <tbody>
                <tr style={{ verticalAlign: 'middle', fontSize: '30px'  }} >
                    <td>{ hat.fabric }</td>
                    <td>{ hat.style }</td>
                    <td>{ hat.color }</td>
                    <td>
                        <img src={hat.picture_url} alt="Hat" style={{ width: '200px', height: '200px' }} />
                    </td>
                    <td>{ hat.location.closet_name }</td>
                </tr>
            </tbody>
        </table>
        );
}

  export default HatDetail;
